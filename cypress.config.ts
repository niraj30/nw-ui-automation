import { defineConfig } from "cypress";

export default defineConfig({
  projectId: "hbgzip",
  viewportWidth: 1600,
  viewportHeight: 1400,
  chromeWebSecurity: false,
  defaultCommandTimeout: 9000,
  reporter: "cypress-multi-reporters",
  reporterOptions: {
    reporterEnabled: "cypress-mochawesome-reporter,mochawesome",
    cypressMochawesomeReporterReporterOptions: {
      reportDir: "cypress/results/reports",
      reportFilename: "report",
      html: true,
      overwrite: true,
      embeddedScreenshots: true,
      inlineAssets: true,
    },
    mochawesomeReporterOptions: {
      reportDir: "cypress/results/reports",
      reportFilename: "report",
      overwrite: true,
      html: false,
      json: true,
      charts: false,
      quiet: true,
      consoleReporter: "none",
    },
  },
  screenshotsFolder: "cypress/results/screenshots",
  videosFolder: "cypress/results/videos",
  video: true,
  trashAssetsBeforeRuns: false,
  watchForFileChanges: false,
  e2e: {
    baseUrl: "https://jsonplaceholder.typicode.com",
    testIsolation: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
