import { Users } from "cypress/support/apiServices/Users";
import { Posts } from "cypress/support/apiServices/Posts";
import { Response } from "cypress/support/utils/types";

const testFile = Cypress.env("testFile");
/* eslint-disable */
const data = require("../../fixtures/apiTestData/" + testFile + ".json");
let apiResponse: Response;

const users = new Users();
const posts = new Posts();

for (let counter = 0; counter < data.testScenarios.length; counter++) {
  const testScenario = data.testScenarios[counter];
  if (testScenario.test === "addPostsFromUserName") {
    describe("Adding New Posts From User Name", () => {
      before(() => {
        users
          .getAllUsers()
          .then((response) => {
            for (let count = 0; count < response.body.length; count++) {
              const userInfo = response.body[count];
              if (userInfo.name === testScenario.userName) {
                testScenario.postInfo.userId = userInfo.id;
                return testScenario.postInfo;
              }
            }
          })
          .then((postInfo) => {
            posts.addNewPost(postInfo).then((response) => {
              apiResponse = response;
            });
          });
      });

      it("Checking Status Code ", () => {
        expect(apiResponse.status).to.eq(201);
      });

      it("Checking New Post ID ", () => {
        expect(apiResponse.body.id).to.not.eq(undefined);
      });

      it("Checking New Post title", () => {
        expect(apiResponse.body.title).to.eq(testScenario.postInfo.title);
      });

      it("Checking New Post body", () => {
        expect(apiResponse.body.body).to.eq(testScenario.postInfo.body);
      });
    });
  }

  if (testScenario.test === "updatePost") {
    describe("Update Post Title and Body", () => {
      before(() => {
        posts.updatePost(testScenario.postInfo).then((response) => {
          apiResponse = response;
        });
      });

      it("Checking Status Code", () => {
        expect(apiResponse.status).to.eq(200);
      });

      it("Checking Post New title", () => {
        expect(apiResponse.body.title).to.eq(testScenario.postInfo.title);
      });

      it("Checking Post New body", () => {
        expect(apiResponse.body.body).to.eq(testScenario.postInfo.body);
      });
    });
  }

  if (testScenario.test === "deletePost") {
    describe("Delete Post", () => {
      before(() => {
        posts.deletePost(testScenario.postId).then((response) => {
          apiResponse = response;
        });
      });

      it("Checking Status Code", () => {
        expect(apiResponse.status).to.eq(200);
      });
    });
  }
}
