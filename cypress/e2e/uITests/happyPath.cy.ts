import { HomePage } from "../../support/pageObjects/homepage/HomePage";
import { Inventory } from "../../support/pageObjects/inventory/Inventory";
import { Cart } from "../../support/pageObjects/cart/Cart";
import { Checkout } from "../../support/pageObjects/checkout/Checkout";
import { Path } from "../../support/utils/paths";

const testFile = Cypress.env("testFile");
/* eslint-disable */
const data = require("../../fixtures/uiTestData/" + testFile + ".json");

const homePage = new HomePage();
const inventory = new Inventory();
const cart = new Cart();
const checkout = new Checkout();

describe("Tests Suite: Swag Labs", () => {
  it("Opening SauceDemo Web page", () => {
    cy.visit("https://www.saucedemo.com/");
  });

  it("Logging In User", () => {
    homePage.loginUser(data.userEmail, data.userPassword);
    cy.url().should("include", Path.INVENTORY);
  });

  for (let counter = 0; counter < data.actions.length; counter++) {
    const userAction = data.actions[counter];

    if (userAction.action === "sortProducts") {
      it("Sort products", () => {
        inventory.sortInventory(userAction.sortType);
      });
    }
    if (userAction.action === "addProducts") {
      it("Add Products to Cart", () => {
        inventory.addToCart(userAction.productList);
        cart.verifyCartBadgeCount(userAction.cartCount);
      });
    }

    if (userAction.action === "verifyCart") {
      it("Verify Cart", () => {
        inventory.navigateToUserCart();
        cy.url().should("include", Path.CART);
        cart.verifyCart(userAction.productListInCart);
      });
    }

    if (userAction.action === "removeProduct") {
      it("Removing Product from Cart", () => {
        cart.removeFromCart(userAction.productList);
        cart.verifyCartBadgeCount(userAction.cartCount);
      });
    }

    if (userAction.action === "continueShopping") {
      it("Continue Shopping", () => {
        cart.continueShopping();
        cy.url().should("include", Path.INVENTORY);
      });
    }

    if (userAction.action === "checkout") {
      it("Checkout", () => {
        inventory.navigateToUserCart();
        cy.url().should("include", Path.CART);
        cart.proceedToCheckout();
        cy.url().should("include", Path.CHECKOUTSTEPONE);
        checkout.checkoutStepOne(userAction.userInfo);
        cy.url().should("include", Path.CHECKOUTSTEPTWO);
        checkout.verifyProductList(userAction.productList);
        checkout.verifyTotalPrice(userAction.totalPrice);
        checkout.clickFinish();
        cy.url().should("include", Path.CHECKOUTCOMPLETE);
      });
    }
  }
});
