import { PostInfo } from "../utils/types";

export class Posts {
  // fetch all post
  public getAllPosts() {
    return cy.request({
      method: "GET",
      url: "/posts",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  // fetch post using ID
  public getPostsById(id: string) {
    return cy.request({
      method: "GET",
      url: `/posts/${id}`,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  //fetech post using UserID
  public getPostsByUserId(userId: string) {
    return cy.request({
      method: "GET",
      url: `/posts/?userId=${userId}`,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  // POST call to add new post
  public addNewPost(postInfo: PostInfo) {
    return cy.request({
      method: "POST",
      url: "/posts",
      body: JSON.stringify({
        title: postInfo.title,
        body: postInfo.body,
        userId: postInfo.userId,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  //PUT call to update existing post
  public updatePost(postInfo: PostInfo) {
    return cy.request({
      method: "PUT",
      url: `/posts/${postInfo.id}`,
      body: JSON.stringify({
        id: postInfo.id,
        title: postInfo.title,
        body: postInfo.body,
        userId: postInfo.userId,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  // DELETE call using post id
  public deletePost(postId: string) {
    return cy.request({
      method: "DELETE",
      url: `/posts/${postId}`,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }
}
