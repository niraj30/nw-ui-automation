export class Users {
  // fetch all users for '/users'
  public getAllUsers() {
    return cy.request({
      method: "GET",
      url: "/users",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }

  //fetch user from ID
  public getUserInfoFromID(id: string) {
    return cy.request({
      method: "GET",
      url: `/users/${id}`,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }
}
