export enum Path {
  INVENTORY = "/inventory.html",
  CART = "/cart.html",
  CHECKOUTSTEPONE = "/checkout-step-one.html",
  CHECKOUTSTEPTWO = "/checkout-step-two.html",
  CHECKOUTCOMPLETE = "/checkout-complete.html",
}
