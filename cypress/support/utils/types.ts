/* eslint-disable */
export interface ProductList {
  productName: string;
  quantity: string;
}

export interface UserInfo {
  firstName: string;
  lastName: string;
  postalCode: string;
}

export interface PostInfo {
  id: string;
  title: string;
  body: string;
  userId: string;
}

export interface Response {
  body: any;
  status: number;
}
