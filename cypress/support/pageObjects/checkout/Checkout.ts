import { CheckoutSelectors } from "./selectors";
import { ProductList, UserInfo } from "cypress/support/utils/types";

export class Checkout {
  // fill step-one checkout details
  public checkoutStepOne(userInfo: UserInfo) {
    cy.get(CheckoutSelectors.userFirstNameSelector).type(userInfo.firstName);
    cy.get(CheckoutSelectors.userLastNameSelector).type(userInfo.lastName);
    cy.get(CheckoutSelectors.userPostalCodeSelector).type(userInfo.postalCode);
    cy.get(CheckoutSelectors.continueBtnSelector).click();
  }

  // check product list
  public verifyProductList(productList: ProductList[]) {
    for (let counter = 0; counter < productList.length; counter++) {
      cy.get(CheckoutSelectors.itemSelector)
        .eq(counter)
        .within(() => {
          cy.get(CheckoutSelectors.itemQuantitySelector).should(
            "have.text",
            productList[counter].quantity
          );
          cy.get(CheckoutSelectors.itemNameSelector).should(
            "have.text",
            productList[counter].productName
          );
        });
    }
  }

  // click on finish button
  public clickFinish() {
    cy.get(CheckoutSelectors.finishBtnSelector).click();
  }

  // check total price
  public verifyTotalPrice(totalPrice: string) {
    cy.get(CheckoutSelectors.totalPriceSelector).should(
      "have.text",
      totalPrice
    );
  }
}
