export abstract class CheckoutSelectors {
  static userFirstNameSelector = "#first-name";
  static userLastNameSelector = "#last-name";
  static userPostalCodeSelector = "#postal-code";
  static continueBtnSelector = "#continue";
  static itemSelector = 'div[class="cart_list"] div[class="cart_item"]';
  static itemQuantitySelector = 'div[class="cart_quantity"]';
  static itemNameSelector = 'div[class="inventory_item_name"]';
  static finishBtnSelector = "#finish";
  static totalPriceSelector =
    'div[class="summary_info_label summary_total_label"]';
}

export default {
  ...CheckoutSelectors,
};
