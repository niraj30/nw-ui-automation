export abstract class InventorySelectors {
  static sortInventorySelector = 'select[data-test="product_sort_container"]';
  static cartSelector = 'a[class="shopping_cart_link"]';
  static shoppingCartBadgeSelector = 'span[class="shopping_cart_badge"]';
  static getAddToCartButtonSelector(itemName: string) {
    itemName = itemName.replace(/ /g, "-");
    itemName = itemName.toLocaleLowerCase();
    return 'button[name="add-to-cart-' + itemName + '"]';
  }
}

export default {
  ...InventorySelectors,
};
