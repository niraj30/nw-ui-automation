import { InventorySelectors } from "./selectors";

export class Inventory {
  // choose sort type
  public sortInventory(sortType: string) {
    cy.get(InventorySelectors.sortInventorySelector).select(sortType);
  }

  // add products to cart
  public addToCart(productsList: string[]) {
    for (let counter = 0; counter < productsList.length; counter++) {
      const product = productsList[counter];
      cy.get(InventorySelectors.getAddToCartButtonSelector(product)).click();
    }
  }

  // click on cart button from inventory page
  public navigateToUserCart() {
    cy.get(InventorySelectors.cartSelector).click();
  }
}
