export abstract class CartSelectors {
  static cartItemSelector = 'div[class="cart_list"] div[class="cart_item"]';
  static itemQuantitySelector = 'div[class="cart_quantity"]';
  static itemNameSelector = 'div[class="inventory_item_name"]';
  static continueShoppingSelector = "#continue-shopping";
  static checkoutSelector = "#checkout";
  static shoppingCartBadgeSelector = 'span[class="shopping_cart_badge"]';

  static getRemoveFromCartSelector(itemName: string) {
    itemName = itemName.replace(/ /g, "-");
    itemName = itemName.toLocaleLowerCase();
    return 'button[name="remove-' + itemName + '"]';
  }
}

export default {
  ...CartSelectors,
};
