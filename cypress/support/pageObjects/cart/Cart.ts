import { CartSelectors } from "./selectors";
import { ProductList } from "cypress/support/utils/types";

export class Cart {
  // loop through every item in cart and check product name & quantity.
  public verifyCart(productListInCart: ProductList[]) {
    for (let counter = 0; counter < productListInCart.length; counter++) {
      cy.get(CartSelectors.cartItemSelector)
        .eq(counter)
        .within(() => {
          cy.get(CartSelectors.itemQuantitySelector).should(
            "have.text",
            productListInCart[counter].quantity
          );
          cy.get(CartSelectors.itemNameSelector).should(
            "have.text",
            productListInCart[counter].productName
          );
        });
    }
  }

  // remove items from cart
  public removeFromCart(productsList: string[]) {
    for (let counter = 0; counter < productsList.length; counter++) {
      const product = productsList[counter];
      cy.get(CartSelectors.getRemoveFromCartSelector(product)).click();
    }
  }
  // check badge count
  public verifyCartBadgeCount(cartCount: string) {
    cy.get(CartSelectors.shoppingCartBadgeSelector).should(
      "have.text",
      cartCount
    );
  }

  // continue shopping action
  public continueShopping() {
    cy.get(CartSelectors.continueShoppingSelector).click();
  }

  // click on check out button
  public proceedToCheckout() {
    cy.get(CartSelectors.checkoutSelector).click();
  }
}
