import { HomePageSelectors } from "./selectors";

export class HomePage {
  //login screen
  public loginUser(id: string, password: string) {
    cy.get(HomePageSelectors.userNameSelector).type(id);
    cy.get(HomePageSelectors.passwordSelector).type(password);
    cy.get(HomePageSelectors.loginBtnSelector).click();
  }
}
