export abstract class HomePageSelectors {
  static userNameSelector = "#user-name";
  static passwordSelector = "#password";
  static loginBtnSelector = "#login-button";
}

export default {
  ...HomePageSelectors,
};
