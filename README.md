## UI Test

```
git clone https://gitlab.com/niraj30/nw-ui-automation.git
Open terminal
cd nw-ui-automation
npm i
npx cypress run --spec=cypress/e2e/uITests/happyPath.cy.ts --env testFile=swagLabsTestData --browser chrome --headed
```

## API Test

```
git clone https://gitlab.com/niraj30/nw-ui-automation.git
Open terminal
cd nw-ui-automation
npm i
npx cypress run --spec=cypress/e2e/apiTests/postsApiTest.cy.ts --env testFile=postsApiTestData --browser chrome
```

## Gitlab CI

```
 Consist of two stages 'build' - to install neccesary dependencies 
 & 'test' - Runs the actual tests in CI and generates html using mocha awesome reports generator & videos.
 Also, uploads the artifacts to ci server. 
 
Steps to Run in Pipeline :

Navigate to project home page in Gitlab.
Click on CI/CD 
Click on Pipeline to run the tests in pipeline.

How to access/look project artifacts in Gitlab:

Navigate to Jobs section.
Under Job artifacts, click on browse & navigate to open videos or html report. 

```